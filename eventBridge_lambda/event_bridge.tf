## Locals variables Argentina
## suffix _ar = infra Argentina
locals {
  # IMPORTANT!!!!!!!!: change the name of this variables when it is put into production  
  event_bridge_name_retry_price        = "${local.country}-ccom-pim-retry-price-exporter"
  event_bridge_name_retry_product      = "${local.country}-ccom-pim-retry-product-exporter"
  event_bridge_description_retry_price = "Scheduled retry every 5 min of price event processing"

  retry_interval = 5
}

#=============== RETRY PRICE =================
resource "aws_cloudwatch_event_rule" "event_bridge_name_retry_price" {
  name                = "${local.event_bridge_name_retry_price}-rule"
  description         = local.event_bridge_description_retry_price
  schedule_expression = "rate(${local.retry_interval} minutes)"
}

resource "aws_cloudwatch_event_target" "event_bridge_name_retry_price_target" {
  rule = aws_cloudwatch_event_rule.event_bridge_name_retry_price.name
  arn  = module.lambda_container_retry_price_exporter.lambda_function_arn
  input = jsonencode(
    {
      eventType : "PRICE"
  })
}


#Lambda permission 
resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda_retry" {
  statement_id  = "AllowExecutionFromCloudWatch-price"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda_container_retry_price_exporter.lambda_function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.event_bridge_name_retry_price.arn
}

# resource "aws_iam_role" "event_bridge_name_retry_price_role" {
#   name = "${local.event_bridge_name_retry_price}-role"

#   assume_role_policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action = "sts:AssumeRole"
#         Principal = {
#           Service = "lambda.amazonaws.com"
#         }
#         Effect = "Allow"
#       }
#     ]
#   })
# }

# resource "aws_iam_policy" "allow_cloudwatch_to_call_lambda_retry_price_policy" {
#   name = "${local.event_bridge_name_retry_price}-policy"
#   policy = jsonencode({
#     Version = "2012-10-17",
#     Statement = [
#       {
#         Action   = "lambda:InvokeFunction",
#         Effect   = "Allow",
#         Resource = aws_cloudwatch_event_rule.event_bridge_name_retry_price.arn
#       }
#     ]
#   })
# }

# resource "aws_iam_role_policy_attachment" "allow_cloudwatch_to_call_lambda_retry_price" {
#   role       = aws_iam_role.event_bridge_name_retry_price_role.name
#   policy_arn = aws_iam_policy.allow_cloudwatch_to_call_lambda_retry_price_policy.arn
# }

#=============== RETRY PRODUCT =================

resource "aws_cloudwatch_event_rule" "event_bridge_name_retry_product" {
  name                = "${local.event_bridge_name_retry_product}-rule"
  description         = local.event_bridge_description_retry_price
  schedule_expression = "rate(${local.retry_interval} minutes)"
}

resource "aws_cloudwatch_event_target" "event_bridge_name_retry_product_target" {
  rule = aws_cloudwatch_event_rule.event_bridge_name_retry_product.name
  arn  = module.lambda_container_retry_price_exporter.lambda_function_arn
  input = jsonencode(
    {
      eventType : "PRODUCT"
  })
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda_retry_product" {
  statement_id  = "AllowExecutionFromCloudWatch-product"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda_container_retry_price_exporter.lambda_function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.event_bridge_name_retry_product.arn
}
