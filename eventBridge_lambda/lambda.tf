## Locals variables Argentina
## suffix _ar = infra Argentina
locals {
  # IMPORTANT!!!!!!!!: change the name of this variables when it is put into production  
  lambda_name_retry_exporter_exporter        = "${local.country}-ccom-pim-retry-exporter"
  lambda_description_retry_exporter_exporter = "Test IAC"
}


# ECR for lambda retry Argentina
# Lambda retry exporter
module "lambda_container_retry_price_exporter" {
  source = "terraform-aws-modules/lambda/aws"

  function_name = local.lambda_name_retry_exporter_exporter
  description   = local.lambda_description_retry_exporter_exporter
  handler       = "index.handler"
  runtime       = "nodejs16.x"

  source_path = "src/index.js"
  # lambda_role = aws_iam_role.event_bridge_name_retry_price_role.name

  timeout = 300

  tags = {
    Name = local.lambda_name_retry_exporter_exporter
  }
}
# Policy for the lambda-retry can push the image to the ECR

# Policy for product, media and price queues execute actions on the lambda-retry
# resource "aws_iam_role_policy" "sqs_splitter_event_policy" {
#   name       = "sqs_splitter_permission_ar"
#   role       = module.lambda_container_retry_price_exporter.lambda_function_arn
#   depends_on = [module.lambda_container_retry_price_exporter]

#   policy = <<POLICY
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": [
#         "sqs:DeleteMessage",
#         "sqs:GetQueueAttributes",
#         "sqs:GetQueueUrl",
#         "sqs:SendMessage",
#         "sqs:SetQueueAttributes",
#         "sqs:ReceiveMessage"
#       ],
#       "Effect": "Allow",
#       "Resource": [
#         "${aws_sqs_queue.sqs_queue_splitter_event_price_spid.arn}",
#       ]
#     }
#   ]
# }
# POLICY
# }

# Policies to lambda retry has access to dynamoDb

