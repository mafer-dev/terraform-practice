provider "aws" {
  region = var.aws_region
}

resource "aws_s3_bucket" "webtest1" {
  bucket = var.bucket_name
}

resource "aws_s3_bucket_acl" "bucketacl1" {
  bucket = aws_s3_bucket.webtest1.id
  acl    = "public-read"
}

resource "aws_s3_bucket_website_configuration" "example" {
  bucket = aws_s3_bucket.webtest1.bucket

  index_document {
    suffix = "index.html"
  }
}

resource "aws_s3_object" "object1" {
  bucket = aws_s3_bucket.webtest1.bucket
  key    = "index.html"
  acl = "public-read"
  content = var.content
  content_type = "text/html"
}