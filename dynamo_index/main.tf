locals {
  table_read_capacity  = 10
  table_write_capacity = 10
  billing_mode         = "PAY_PER_REQUEST" // PAY_PER_REQUEST | PROVISIONED

  ar_brand_exporter = {
    partition_key   = "id"
    attr_brand_id   = "brand_id"
    attr_channel_id = "channel_id"
  }
  ar_brand_exporter_index1 = {
    name          = "${local.ar_brand_exporter.attr_brand_id}__${local.ar_brand_exporter.attr_channel_id}__index"
    partition_key = local.ar_brand_exporter.attr_brand_id
    sort_key      = local.ar_brand_exporter.attr_channel_id
  }
}

provider "aws" {
  region = "us-east-1"
}

#DYNAMO
resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name         = "ar-brand-exporter"
  billing_mode = local.billing_mode
  hash_key     = local.ar_brand_exporter.partition_key

  attribute {
    name = local.ar_brand_exporter.partition_key
    type = "S"
  }

  attribute {
    name = local.ar_brand_exporter.attr_brand_id
    type = "S"
  }

  attribute {
    name = local.ar_brand_exporter.attr_channel_id
    type = "S"
  }

  #for autoscaling
  lifecycle {
    ignore_changes = [write_capacity, read_capacity]
  }

  global_secondary_index {
    name            = local.ar_brand_exporter_index1.name
    hash_key        = local.ar_brand_exporter_index1.partition_key ##Partition key
    range_key       = local.ar_brand_exporter_index1.sort_key      ## Sort key
    projection_type = "ALL"
  }

  tags = {
    Name        = "dynamodb-table-1"
    Environment = "production"
  }
}
