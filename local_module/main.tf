provider "aws" {
  region = "us-east-1"
}

module "localvpc" {
  source = "./vpc"

  // Variables input type
  vpc_cidr = "10.50.0.0/16"
  vpc_name = "Name from local module"
}

  //variables output type
output "vpc_output" {
    value = module.localvpc.vpcid
}