resource "aws_sns_topic" "sns_topic" {
  name            = var.sns_name
  
  policy = jsonencode({
    Version = "2012-10-17"
    Id = "1"
    Statement = [
      {
        Sid = "2"
        Effect = "Allow"
        Principal = {
          "AWS": var.account
        }
        Action = [
          "SNS:Subscribe",
          "SNS:Publish",
          "SNS:Receive"
        ]
        Resource = "arn:aws:sns:${var.aws_region}:${var.account}:${var.sns_name}"
      },
      {
        Sid     = "3"
        Effect  = "Allow"
        Action   = "SNS:Publish"
        Principal = { 
            Service =  "s3.amazonaws.com" 
        },
        Resource = "arn:aws:sns:${var.aws_region}:${var.account}:${var.sns_name}"
        Condition = {
            StringEquals = {
                "aws:SourceAccount" = var.account
            },
            ArnLike = {
                "aws:SourceArn" = "arn:aws:s3:::${var.s3_bucket_name}"
            }
        }
      }
    ]
  })
}

#s3
# Exist Bucket
# Add notification configuration to SNS
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = var.s3_bucket_name

  topic {
    topic_arn     = aws_sns_topic.sns_topic.arn
    events        = ["s3:ObjectCreated:*"]
  }
}

## SNS notification SQS 
resource "aws_sns_topic_subscription" "sns_notification_queue_event" {
  topic_arn = "arn:aws:sns:${var.aws_region}:${var.account}:${var.sns_name}"
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.sqs_queue.arn

#   protocol  = "lambda"
#   endpoint  = module.lambda_function.lambda_function_arn
}

# Resource that allows the lambda to obtain events from sns
# resource "aws_lambda_permission" "allow_sns_invoke" {
#   statement_id  = "AllowExecutionFromSNS"
#   action        = "lambda:InvokeFunction"
#   function_name = module.lambda_function.lambda_function_name
#   principal     = "sns.amazonaws.com"
#   source_arn    = aws_sns_topic.sns_topic.arn
# }
