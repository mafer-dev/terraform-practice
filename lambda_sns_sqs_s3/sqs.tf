resource "aws_sqs_queue" "sqs_queue_dlq" {
  count = 1 

  name = "${var.sqs_name}-dlq"

  # FIFO settings
  fifo_queue                  = false
  content_based_deduplication = false

  #Settings
  visibility_timeout_seconds =  180  # dlq_visibility_timeout
  message_retention_seconds  = 86400 # dlq_message_retention
  max_message_size           = 2048
  delay_seconds              = 1 # dlq_delay
  receive_wait_time_seconds  = 15 # dlq_polling

  # Server-side encryption (SSE) for a queue using SQS-owned encryption keys
  sqs_managed_sse_enabled = true
  
  tags = {
    Environment = "production"
  }
}


resource "aws_sqs_queue" "sqs_queue" {
  name                      = var.sqs_name

  #Settings
  visibility_timeout_seconds= 360  # Tiempo de espera para que las otros eventos lleguen a la cola y se envie
  message_retention_seconds = 86400
  max_message_size          = 2048
  delay_seconds             = 1  
  receive_wait_time_seconds = 15

  #DLQ
  redrive_policy = jsonencode({
    deadLetterTargetArn = element(concat(aws_sqs_queue.sqs_queue_dlq.*.arn, tolist([""])), 0)
    maxReceiveCount     = 2
  })


  # Server-side encryption (SSE) for a queue using SQS-owned encryption keys
  sqs_managed_sse_enabled = true

  tags = {
    Environment = "production"
  }
}

resource "aws_sqs_queue_policy" "queue_policy" {
  queue_url = aws_sqs_queue.sqs_queue.id

  policy = jsonencode({
    Id = "Policy1610380314922"
    Version = "2012-10-17"
    Statement = [
      {
        Sid =  "Stmt1610380312416"
        Effect   = "Allow"
        Resource = aws_sqs_queue.sqs_queue.arn
        Action = [
          "SQS:DeleteMessage",
          "SQS:SendMessage",
          "SQS:ReceiveMessage",
          "SQS:GetQueueAttributes"
        ]
        Principal = {
          Service = "sns.amazonaws.com"
        }
      }
    ]
  })
}

# Resource that allows the lambda to obtain events from queue connected to the sns
resource "aws_lambda_event_source_mapping" "sqs_event_source_mapping" {
  event_source_arn = aws_sqs_queue.sqs_queue.arn
  function_name = module.lambda_function.lambda_function_arn
  batch_size = 10
  maximum_batching_window_in_seconds = 30
  function_response_types = [ "ReportBatchItemFailures" ]
}