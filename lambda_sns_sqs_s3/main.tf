provider "aws" {
  region = var.aws_region
}

#SQS 

#Lambda
module "lambda_function" {
  source = "terraform-aws-modules/lambda/aws"

  function_name = var.lambda_name
  description   = var.lambda_description
  handler       = "index.handler"
  runtime       = "nodejs16.x"

  source_path = "src/index.js"
  
  timeout = 300

  tags = {
    Name = var.lambda_name
  }
}

resource "aws_iam_role_policy" "sqs_lambda_event_policy" {
  name = "${var.lambda_name}-sqs-permission"
  role = module.lambda_function.lambda_role_name

  depends_on = [module.lambda_function]

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "SQS:DeleteMessage",
        "SQS:SendMessage",
        "SQS:ReceiveMessage",
        "SQS:GetQueueAttributes"
      ],
      "Effect": "Allow",
      "Resource": "${aws_sqs_queue.sqs_queue.arn}"
    }
  ]
}
POLICY
}