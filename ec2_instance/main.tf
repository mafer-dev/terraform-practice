provider "aws" {
    region = "us-east-1"
}

resource "aws_instance" "instance1" {
  ami = "ami-0022f774911c1d690"
  instance_type = "t1.micro"
  tags = {
    "Name" = "first_instance"
  }
}